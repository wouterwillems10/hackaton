local platform = {};

function platform.create(x,y, camera, verticals, theme)
    local platformEntity = display.newRect(x, y, 180, 180);
    platformEntity.alpha = 0;
    local entity = {};

    function entity.getRect()
        return platformEntity;
    end

    local background;
    if(theme == 'ice') then
        background = "ice_block.png";
    else
        background = "vuur_block.png";
    end


    local image = display.newImageRect( background, 180, 180);
    --myImage:translate( myImage.width/2*4, myImage.height/2*4 );
    camera:insert(platformEntity);
    camera:insert(image);

    image.x = platformEntity.x;
    image.y = platformEntity.y;

    local platformEntitySide = display.newRect(x - platformEntity.width/2 - 10, y, 20, 140);
    platformEntitySide.alpha=0;
    platformEntitySide.dir='left';
    camera:insert(platformEntitySide);
    table.insert(verticals, platformEntitySide);

    local platformEntitySide = display.newRect(x + platformEntity.width/2 + 10, y, 20, 140);
    platformEntitySide.alpha=0;
    platformEntitySide.dir='right';
    camera:insert(platformEntitySide);
    table.insert(verticals, platformEntitySide);

    return entity;
end

function platform.createFlag(x,y, camera, col)
    local platformEntity = display.newRect(x, y, 180, 180);
    platformEntity.alpha = 0;
    local entity = {};

    function entity.getRect()
        return platformEntity;
    end

    local sheetOptions =
    {
        width = 158,
        height = 316,
        numFrames = 16
    }

    local imageSheet = graphics.newImageSheet( "flag.png", sheetOptions);
    local sequences_runningCat = {
        -- consecutive frames sequence
        {
            name = "wave",
            start = 1,
            count = 8,
            time = 800,
            loopCount = 0,
            loopDirection = "forward"
        }
    }
    local animation = display.newSprite( imageSheet, sequences_runningCat )
    animation:scale(1,1);
    camera:insert(animation);

    animation:setSequence( "wave" );
    animation:play();

    --myImage:translate( myImage.width/2*4, myImage.height/2*4 );
    camera:insert(platformEntity);
    camera:insert(animation);

    animation.x = platformEntity.x;
    animation.y = platformEntity.y;

    return entity;
end

return platform;