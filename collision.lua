local inspect = require "inspect";

local collision = {};

function collision.doRectsIntersect(rect1, rect2)
    local vect1 = collision.toVectors(rect1);
    local vect2 = collision.toVectors(rect2);
    return collision.doPolygonsIntersect(vect1, vect2);
end

function collision.rotateRectangleVertex(rect, vertex)
    local rectX, rectY = rect:localToContent( 0, 0 );

    local tempX = vertex.x - rectX;
    local tempY = vertex.y - rectY;

    local theta = math.rad(rect.rotation);
    --    // now apply rotation
    local rotatedX = tempX*math.cos(theta) - tempY*math.sin(theta);
    local rotatedY = tempX*math.sin(theta) + tempY*math.cos(theta);

    --    // translate back
    local x = rotatedX + rectX;
    local y = rotatedY + rectY;

    return {x=x, y=y};
end

function collision.toVectors(rect)
    local rectX, rectY = rect:localToContent( 0, 0 );
    local topLeft = {
        x=rectX - rect.width/2,
        y=rectY - rect.height/2,
    };
    local topRight = {
        x=rectX + rect.width/2,
        y=rectY - rect.height/2,
    };
    local bottomRight = {
        x=rectX + rect.width/2,
        y=rectY + rect.height/2,
    };
    local bottomLeft = {
        x=rectX - rect.width/2,
        y=rectY + rect.height/2,
    };


    local result = {
        collision.rotateRectangleVertex(rect, topLeft),
        collision.rotateRectangleVertex(rect, topRight),
        collision.rotateRectangleVertex(rect, bottomRight),
        collision.rotateRectangleVertex(rect, bottomLeft)
    };
    return result;
end



--[[
* Helper function to determine whether there is an intersection between the two polygons described
* by the lists of vertices. Uses the Separating Axis Theorem
*
* @param a an array of connected points [{x:, y:}, {x:, y:},...] that form a closed polygon
* @param b an array of connected points [{x:, y:}, {x:, y:},...] that form a closed polygon
* @return true if there is any intersection between the 2 polygons, false otherwise
*/
--]]
function collision.doPolygonsIntersect(a, b)
    local polygons = { a, b };
    local minA, maxA, projected, i, i1, j, minB, maxB;

    for i = 1, #polygons do
        --// for each polygon, look at each edge of the polygon, and determine if it separates
        --// the two shapes
        local polygon = polygons[i];
        for i1 = 0, (#polygon-1) do
            --// grab 2 vertices to create an edge
            local i2 = (i1 + 1) % (#polygon);
            local p1 = polygon[i1+1];
            local p2 = polygon[i2+1];

            --// find the line perpendicular to this edge
            local normal = { x = p2.y - p1.y, y = p1.x - p2.x };
            minA = nil;
            maxA = nil;

            --// for each vertex in the first shape, project it onto the line perpendicular to the edge
            --// and keep track of the min and max of these values
            for j = 1, #a do
                projected = normal.x * a[j].x + normal.y * a[j].y;
                if (minA == nil or projected < minA) then
                    minA = projected;
                end
                if (maxA == nil or projected > maxA) then
                    maxA = projected;
                end
            end

            --// for each vertex in the second shape, project it onto the line perpendicular to the edge
            --// and keep track of the min and max of these values
            minB = nil;
            maxB = nil;
            for j = 1, #b do
                projected = normal.x * b[j].x + normal.y * b[j].y;
                if (minB == nil or projected < minB) then
                    minB = projected;
                end
                if (maxB == nil or projected > maxB) then
                    maxB = projected;
                end
            end

            if (maxA < minB or maxB < minA) then
                return false;
            end
        end
    end

    return true;
end

return collision;