local platformFactory = require "platform";
local player = require "player2";
local controls = require "controls2";
local composer = require( "composer" )
local gameTimer = require "gameTimer";
local collision = require "collision";

local scene = composer.newScene()



-- -----------------------------------------------------------------------------------
-- Code outside of the scene event functions below will only be executed ONCE unless
-- the scene is removed entirely (not recycled) via "composer.removeScene()"
-- -----------------------------------------------------------------------------------




-- -----------------------------------------------------------------------------------
-- Scene event functions
-- -----------------------------------------------------------------------------------

-- create()

local gameLoopFn;
local myText;

function scene:create( event )

    local theme = gameTimer.getCoors().theme;
    print('thjeme');
    print(theme);

    local sceneGroup = self.view

    local horizontals = {};
    local verticals = {};
    local camera = display.newGroup();

    -- Code here runs when the scene is first created but has not yet appeared on screen
    sceneGroup:insert(camera);

    local background;
    if(theme == 'ice') then
        background = "ice_background.png";
    else
        background = "vuur_background.png";
    end


    local image = display.newImageRect( background, 3840, 1920);
    --myImage:translate( myImage.width/2*4, myImage.height/2*4 );
    image.x = 3840/2 - 1000;
    image.y = 1920/2;
    camera:insert(image);

    local image2 = display.newImageRect( background, 3840, 1920);
    image2.x = image.x + image.width;
    image2.y = 1920/2;
    camera:insert(image2);

    local image3 = display.newImageRect( background, 3840, 1920);
    image3.x = image2.x + image2.width;
    image3.y = 1920/2;
    camera:insert(image3);

    function addBlock(x,y)
        local test = platformFactory.create(x, y, camera, verticals, theme);
        table.insert(horizontals, test);
    end

    local flag;

    function makeLevel0()
        addBlock(180, 1260);
        addBlock(360, 1260);
        addBlock(540, 1260);

        addBlock(900, 1260);
        addBlock(1080, 1260);
        addBlock(1260, 1350);
        addBlock(1620, 1260);
        addBlock(1800, 1260);

        addBlock(2160, 1260);
        addBlock(2340, 990);
        addBlock(2520, 900);

        addBlock(2790, 900);
        addBlock(2970, 900);
        addBlock(3150, 900);
        addBlock(3330, 900);
        addBlock(3510, 900);

        addBlock(3960, 900);
        addBlock(4140, 900);
        addBlock(4320, 900);

        addBlock(4590, 900);
        addBlock(4770, 900);
        addBlock(4950, 900);

        addBlock(5130, 1170);
        addBlock(5310, 1170);

        addBlock(5580, 1170);
        addBlock(5760, 1170);
        addBlock(5940, 1170);

        addBlock(6390, 1170);

        flag = platformFactory.createFlag(3150, 900 - 200, camera);
    end

    function makeLevel1()
        addBlock(180, 1260);
        addBlock(360, 1260);
        addBlock(540, 1260);
        addBlock(900, 1260);

        addBlock(1260, 1350);
        addBlock(1620, 1260);
        addBlock(1800, 1260);

        addBlock(2160, 1260);

        addBlock(2340, 990);
        addBlock(2520, 900);
        addBlock(2880, 900);
        addBlock(3150, 900);

        addBlock(3420, 990);
        addBlock(3690, 900);
        addBlock(3960, 990);
        addBlock(4140, 990);
        addBlock(4320, 1170);
        addBlock(4500, 1350);
        addBlock(4680, 1530);
        addBlock(4950, 1710);
        addBlock(5220, 1710);
        addBlock(5400, 1620);
        addBlock(5580, 1530);
        addBlock(5760, 1350);
        addBlock(5940, 1170);
        addBlock(6120, 1350);
        addBlock(6300, 1710);

        flag = platformFactory.createFlag(3150, 900 - 200, camera);
    end

    function makeLevel2()
        addBlock(180, 720);
        addBlock(360, 720);
        addBlock(540, 720);

        addBlock(900, 1260);
        addBlock(1170, 1080);
        addBlock(1350, 1080);

        addBlock(1530, 1080);
        addBlock(1710, 1080);
        addBlock(1830, 1080);

        addBlock(2160, 990);
        addBlock(2430, 810);

        addBlock(2700, 630);
        addBlock(2970, 450);
        addBlock(3150, 450);

        addBlock(3330, 540);
        addBlock(3510, 540);
        addBlock(3690, 450);
        addBlock(3870, 360);
        addBlock(4050, 540);
        addBlock(4230, 360);
        addBlock(4410, 540);
        addBlock(4590, 720);
        addBlock(4770, 720);
        addBlock(4950, 900);
        addBlock(5130, 1260);
        addBlock(5310, 1440);
        addBlock(5490, 1440);
        addBlock(5670, 1620);
        addBlock(5850, 1440);
        addBlock(6030, 1440);
        addBlock(6390, 1800);

        flag = platformFactory.createFlag(3150, 450- 200, camera);
    end

    function makeLevel3()
        addBlock(180, 1620);
        addBlock(360, 1440);
        addBlock(540, 1260);
        addBlock(720, 1080);

        addBlock(1080, 1260);
        addBlock(1350, 1260);
        addBlock(1620, 1260);

        addBlock(1890, 1080);
        addBlock(2070, 1620);
        addBlock(2250, 1440);

        addBlock(2250, 1440);
        addBlock(2430, 1620);
        addBlock(2610, 1440);

        addBlock(2790, 1260);
        addBlock(2790, 1440);
        addBlock(3150, 1620);

        addBlock(3330, 1530);
        addBlock(3510, 1350);
        addBlock(3780, 1350);
        addBlock(4050, 1350);
        addBlock(4230, 1170);
        addBlock(4410, 990);
        addBlock(4590, 810);
        addBlock(4860, 810);
        addBlock(5040, 630);
        addBlock(5310, 450);
        addBlock(5490, 450);
        addBlock(5670, 1620);
        addBlock(5940, 1620);
        addBlock(6210, 1440);
        addBlock(6390, 1260);

        flag = platformFactory.createFlag(6390, 1260 - 200, camera);
    end
    --[[
        function makeLevel4()
            addBlock(180, 1080);
            addBlock(360, 1080);
            addBlock(540, 1260);
            addBlock(540, 810);

            addBlock(720, 540);
            addBlock(720, 720);

            addBlock(1080, 1260);

            addBlock(1350, 720);

            addBlock(1440, 1620);
            addBlock(1620, 1440);

             addBlock(1980, 1080);

            addBlock(2160, 810);
            addBlock(2430, 540);
            addBlock(2610, 1620);

            addBlock(2790, 1440);
            addBlock(2790, 1260);
            addBlock(3150, 1260);

            addBlock(3330, 1170);
            addBlock(3510, 990);
            addBlock(3780, 990);
            addBlock(3960, 810);
            addBlock(4140, 810);
            addBlock(4320, 810);
            addBlock(4500, 630);
            addBlock(4770, 810);
            addBlock(4950, 1170);
            addBlock(5130, 1530);
            addBlock(5310, 1800);
            addBlock(5580, 1800);
            addBlock(5850, 1620);
            addBlock(6120, 1440);
            addBlock(6390, 1260);

            flag = platformFactory.createFlag(6390, 1260 - 200, camera);
        end
    ]]


    function makeLevel4()

        addBlock(360, 1170);
        addBlock(540, 1080);
        addBlock(540, 900);
        addBlock(720, 810);
        addBlock(720, 630);
        addBlock(900, 540);
        addBlock(900, 360);
        addBlock(1080, 630);
        addBlock(1260, 630);

        addBlock(1440, 1710);
        addBlock(1620, 1710);
        addBlock(1620, 1530);
        addBlock(1800, 1530);
        addBlock(1800, 1350);
        addBlock(1980, 1350);
        addBlock(1980, 1170);
        addBlock(2160, 1080);
        addBlock(2160, 900);

        addBlock(2430, 1710);
        addBlock(2610, 1710);
        addBlock(2790, 1710);
        addBlock(2790, 1530);
        addBlock(2970, 1530);
        addBlock(2970, 1350);

        addBlock(3150, 1350);
        addBlock(3330, 1350);
        addBlock(3330, 1170);
        addBlock(3510, 1170);
        addBlock(3510, 990);

        addBlock(3780, 990);
        addBlock(3960, 990);
        addBlock(3960, 810);
        addBlock(4140, 810);
        addBlock(4320, 810);
        addBlock(4320, 810);
        addBlock(4500, 810);
        addBlock(4500, 630);

        addBlock(4770, 810);
        addBlock(4950, 1170);
        addBlock(5130, 1530);
        addBlock(5310, 1800);

        addBlock(5580, 1800);

        addBlock(5850, 1620);
        addBlock(6120, 1440);
        addBlock(6300, 1440);
        addBlock(6390, 1260);

        flag = platformFactory.createFlag(6390, 1260 - 200, camera);
    end

    local rand = math.random(5) - 1;
    if(rand == 0) then
        makeLevel0();
    end
    if(rand == 1) then
        makeLevel1();
    end
    if(rand == 2) then
        makeLevel2();
    end
    if(rand == 3) then
        makeLevel3();
    end
    if(rand == 4) then
        makeLevel4();
    end

    myText = display.newText( "level: " .. rand, 200, 400, native.systemFont, 66 );
    myText.text="level: "..rand;
    myText:setFillColor(0,1,0);
    myText.alpha= 0;


    player.create(camera);
    controls.create();

    local function gameLoopTick(msDelta)
        gameTimer.addGameTime(msDelta);

        if (controls.isLeft()) then
            player.setDirection("left");
        elseif (controls.isRight()) then
            player.setDirection("right");
        elseif (controls.isJump()) then
            player.jump();
        elseif (controls.isIdle()) then
            player.setDirection("idle");
        end

        player.tick(msDelta);

        local didOverlap3 = collision.doRectsIntersect(player.getRect(), flag.getRect());
        if (didOverlap3) then
            composer.removeScene( "scene2" );
            composer.gotoScene( "scene1" );
            return;
        end

        for i, platform in pairs(verticals) do
            local didOverlap2 = collision.doRectsIntersect(player.getRect(), platform);
            if (didOverlap2) then
                player.wallSlideObject(platform, platform.dir);
                return;
            end
        end

        local foundOverlap = false;
        for i, platform in pairs(horizontals) do
            local didOverlap = collision.doRectsIntersect(player.getRect(), platform.getRect());
            if (didOverlap and foundOverlap == false) then
                player.putOnObject(platform.getRect(), msDelta);
                foundOverlap = true;
            end
        end

    end

    function moveCamera()
        camera.x = display.contentWidth/2 - player.getRect().x;
    end

    local function gameLoop(event)
        gameLoopTick(20);
        moveCamera();
    end

    gameLoopFn = gameLoop;

    local function advanceFrame()
        gameLoopTick(20);
    end


    --local frameAdv = display.newRect(400,200,80,80);
    --frameAdv:addEventListener('tap', advanceFrame);
    Runtime:addEventListener("enterFrame", gameLoop);
end


-- show()
function scene:show( event )

    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then
        -- Code here runs when the scene is still off screen (but is about to come on screen)

    elseif ( phase == "did" ) then
        -- Code here runs when the scene is entirely on screen

    end
end


-- hide()
function scene:hide( event )

    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then
        -- Code here runs when the scene is on screen (but is about to go off screen)

    elseif ( phase == "did" ) then
        -- Code here runs immediately after the scene goes entirely off screen

    end
end


-- destroy()
function scene:destroy( event )

    local sceneGroup = self.view
    -- Code here runs prior to the removal of scene's view
    print("destroyed 2");
    myText.text=""
    Runtime:removeEventListener( "enterFrame", gameLoopFn )
end


-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------

return scene