local controls = {};
local leftHold = false;
local rightHold = false;

local cursor = display.newRect(0,0,30,30);
cursor:setFillColor(0,1,0);

local myText = display.newText( "Hond!", 200, 200, native.systemFont, 66 )
myText.alpha = 0;
myText:setFillColor( 1, 0, 0 );

function controls.create()
    local ui = display.newRect(display.contentCenterX / 2 - 20, display.contentCenterY, display.contentWidth / 2, display.contentHeight - 200);
    ui:addEventListener("touch", controls.leftUiTouchListener);
    ui.isVisible = false;
    ui.isHitTestable = true;
    local ui2 = display.newRect(display.contentCenterX + display.contentCenterX / 2 + 20, display.contentCenterY, display.actualContentWidth / 2, display.contentHeight - 200);
    ui2:addEventListener("touch", controls.rightUiTouchListener);
    ui2.isVisible = false;
    ui2.isHitTestable = true;
end

function controls.isLeft()
    if (controls.isJump()) then
        return false;
    end
    return leftHold;
end

function controls.isRight()
    if (controls.isJump()) then
        return false;
    end
    return rightHold;
end

function controls.isJump()
    return rightHold and leftHold;
end

function controls.isIdle()
    return not rightHold and not leftHold;
end

function controls.leftUiTouchListener(event)
    leftHold = false;
    cursor.x = event.x;
    cursor.y = event.y;
    myText.text = math.round(cursor.x) .. "-" .. math.round(cursor.y);
    if (event.phase == "began" or event.phase == "moved") then
        leftHold = true;
    end
end

function controls.rightUiTouchListener(event)
    rightHold = false;
    cursor.x = event.x;
    cursor.y = event.y;
    cursor.alpha = 0;
    myText.text = math.round(cursor.x) .. "-" .. math.round(cursor.y);
    if (event.phase == "began" or event.phase == "moved") then
        rightHold = true;
    end
end

return controls;
