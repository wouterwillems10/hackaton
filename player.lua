local collision = require "collision";
local composer = require "composer";
local gameTimer = require "gameTimer";

local playerFactory = {};

local targetDirectionX;
local targetDirectionY;

local currentDirection = nil;

local myText = display.newText( "coors", 800, 200, native.systemFont, 66 );
myText.alpha = 0;
myText:setFillColor(0,1,0);

function playerFactory.create(camera)
    local coors = gameTimer.getCoors();
    local playerEntity;
    playerEntity = display.newRect(0, 0, 80, 60);
    if(coors.x) then
        playerEntity.x = coors.x;
        playerEntity.y = coors.y;
    else
        playerEntity.x = 5800;
        playerEntity.y = 15800;
    end


    playerEntity:setFillColor(1, 0, 0);
    playerEntity.alpha = 0.0;
    camera:insert(playerEntity);
    local entity = {};

    local sheetOptions =
    {
        width = 64,
        height = 64,
        numFrames = 36
    }
    local imageSheet = graphics.newImageSheet( "men.png", sheetOptions);
    local sequences_runningCat = {
        -- consecutive frames sequence
        {
            name = "up",
            start = 1,
            count = 8,
            time = 800,
            loopCount = 0,
            loopDirection = "forward"
        },
        {
            name = "left",
            start = 10,
            count = 8,
            time = 800,
            loopCount = 0,
            loopDirection = "forward"
        },
        {
            name = "down",
            start = 19,
            count = 8,
            time = 800,
            loopCount = 0,
            loopDirection = "forward"
        },
        {
            name = "right",
            start = 28,
            count = 8,
            time = 800,
            loopCount = 0,
            loopDirection = "forward"
        }
    }
    local animation = display.newSprite( imageSheet, sequences_runningCat )
    animation:scale(3.5,3.5);
    camera:insert(animation);

    function entity.getRect()
        return playerEntity;
    end

    function entity.setDirection(x, y)
        targetDirectionX = x;
        targetDirectionY = y;

        local localX, localY = playerEntity:localToContent(0, 0);
        local diffX = targetDirectionX - localX;
        local diffY = targetDirectionY - localY;

        if(math.abs(diffY) > math.abs(diffX)) then
            if(currentDirection ~= 'up' and localY > targetDirectionY) then
                print('up');
                currentDirection = 'up';
                animation:setSequence( "up" );
                animation:play();
            end

            if(currentDirection ~= 'down' and localY < targetDirectionY) then
                print('down');
                currentDirection = 'down';
                animation:setSequence( "down" );
                animation:play();
            end
        else
            if(currentDirection ~= 'left' and localX > targetDirectionX) then
                print('left');
                currentDirection = 'left';
                animation:setSequence( "left" );
                animation:play();
            end

            if(currentDirection ~= 'right' and localX < targetDirectionX) then
                print('right');
                currentDirection = 'right';
                animation:setSequence( "right" );
                animation:play();
            end
        end

    end

    function entity.cancelDirection()
        targetDirectionX = nil;
        targetDirectionY = nil;
        currentDirection = nil;
        animation:pause();
    end

    function entity.tick(ms, walls, thermos)
        local prevX = playerEntity.x;
        local prevY = playerEntity.y;
        local speed = 700;
        local localX, localY = playerEntity:localToContent(0, 0);
        if (targetDirectionX ~= nil and targetDirectionY ~= nil) then
            local diffX = targetDirectionX - localX;
            playerEntity.x = playerEntity.x + (speed*diffX/500 * ms);

            local diffY = targetDirectionY - localY;
            playerEntity.y = playerEntity.y + (speed*diffY/500 * ms);
        end

        local foundCollision = false;
        for i, wall in pairs(walls) do
            local doesCollide = collision.doRectsIntersect(playerEntity, wall);
            if (doesCollide) then
                playerEntity.x = prevX;
                playerEntity.y = prevY;
                foundCollision = true;
            end
        end

        for i, thermo in pairs(thermos) do
            local doesCollide = collision.doRectsIntersect(playerEntity, thermo);
            if (doesCollide) then
                gameTimer.setCoors(prevX,prevY, thermo.theme);
                composer.removeScene( "scene1" );
                composer.gotoScene( "scene2" );
            end
        end

        if(not foundCollision) then
            animation.x = playerEntity.x;
            animation.y = playerEntity.y - 30;

--            local localX, localY = playerEntity:localToContent(0, 0);
--            print(localX, localY);
--            print(playerEntity.x, playerEntity.y);

            myText.text = math.round(playerEntity.x) .. " - " .. math.round(playerEntity.y);
            myText.x = playerEntity.x;
            myText.y = playerEntity.y + 100;
            camera:insert(myText);
        end
    end

    return entity;
end

return playerFactory;