local inspect = require "inspect";
local wall = require "wall";
local collision = require "collision";
local playerFactory = require "player";
local controls = require "controls";
local json = require "json";
system.activate("multitouch");
local composer = require( "composer" )

local scene = composer.newScene()

-- -----------------------------------------------------------------------------------
-- Code outside of the scene event functions below will only be executed ONCE unless
-- the scene is removed entirely (not recycled) via "composer.removeScene()"
-- -----------------------------------------------------------------------------------




-- -----------------------------------------------------------------------------------
-- Scene event functions
-- -----------------------------------------------------------------------------------

-- create()

local gameLoopFn;

function scene:create( event )

    local sceneGroup = self.view
    -- Code here runs when the scene is first created but has not yet appeared on screen


    local camera = display.newGroup();
    sceneGroup:insert(camera);

    local walls = {};
    local thermos = {};

    --local myImage = display.newImage( "map.png" )
    local image = display.newImageRect( "map.png", 2811, 5825);
    --myImage:translate( myImage.width/2*4, myImage.height/2*4 );
    image.x = 2811/2*4;
    image.y = 5825/2*4;
    image:scale(4,4);
    camera:insert(image);
    --camera:insert(myImage);
    --myImage:scale(4,4);
    local player = playerFactory.create(camera);

    local directionListener = function(x, y)
        player.setDirection(x, y);
    end
    local stopListener = function()
        player.cancelDirection();
    end

    local control = controls.create(directionListener, stopListener);

    --print(inspect(sheetInfo:getSheet()));

    function jsonFile( filename, base )
        -- set default base dir if none specified
        if not base then base = system.ResourceDirectory; end

        -- create a file path for corona i/o
        local path = system.pathForFile( filename, base )

        -- will hold contents of file
        local contents

        -- io.open opens a file at path. returns nil if no file found
        local file = io.open( path, "r" )
        if file then
            -- read all contents of file into a string
            contents = file:read( "*a" )
            io.close( file )     -- close the file after using it
        else
            print( "** Error: cannot open file" )
        end

        return contents
    end

    local jsonFileThing = jsonFile("data.json", "/");
    local tempData = json.decode(jsonFileThing);

    for i, entry in pairs(tempData) do
        print("=-=-=--=");

        local tempText = display.newText( "" .. entry.temperature, entry.xCoor, entry.yCoor + 240, native.systemFont, 66 );
        tempText:setFillColor(1,0,0);
        tempText.alpha = 0;
        camera:insert(tempText);

        if(tonumber(entry.temperature) >= 25) then
            local image = display.newImageRect( "vuur_thermo.png", 224, 350);
            image.theme = 'fire';
            table.insert(thermos, image);
            image.x = entry.xCoor;
            image.y = entry.yCoor;
            camera:insert(image);
        else
            local image = display.newImageRect( "ice_thermo.png", 224, 350);
            image.theme = 'ice';
            table.insert(thermos, image);
            image.x = entry.xCoor;
            image.y = entry.yCoor;
            camera:insert(image);
        end

    end


    function addWall(x,y,w,h, a)
        local wall1 = wall.create(x,y, w, h, a);
        table.insert(walls, wall1.getRect());
        camera:insert(wall1.getRect());
    end



    --//x, y, w, h, angle
    --buitenmuren
    addWall(4960,1406,8189,30, -3); --1
    addWall(9579,11432,20478,30, 87); --2
    addWall(9026,21202,2391,30, 22); --3
    addWall(7368,18546,4539,30, 76); --4
    addWall(2672,15453,3566,30, 12); --5a
    addWall(5342,16033,712,30, 12); --5b
    addWall(6478,16286,657,30, 12); --5c

    addWall(892,8349,13413,30, 90); --6
    --grote binnenmoren
    addWall(3632,7691,12382,30, 90); --7
    addWall(3639,15107,1083,30, 90); --8
    addWall(4643,12015,2021,30, -3); --9
    addWall(5915,12965,2075,30, 75); --10a
    addWall(5909,13698,365,30, 2); --10b
    addWall(5489,12862,1721,30, 75); --10c
    addWall(7080,13965,1804,30, 0); --11
    addWall(8887,15000,1773,30, 0); --12

    --blauwe muren
    addWall(8663,16326,2325,30, 0); --13
    addWall(8838,17592,2151,30, 2); --14
    addWall(9022,18957,1905,30, 4); --15
    addWall(9180,20099,1688,30, 3); --16

    --kleine blauwe muren
    addWall(8531,20894,227,30, 78); --17
    addWall(8340,20014,510,30, 78); --18
    addWall(8087,18885,845,30, 78); --19
    addWall(7768,17564,790,30, 78); --20
    addWall(7523,16492,238,30, 78); --21

    --kleine rode muren
    addWall(1116,7149,468,30, -1); --21
    addWall(2081,7150,504,30, -1); --22
    addWall(2339,6912,467,30, 89); --23
    addWall(2991,6688,1231,30, 2); --24

    --kleine gele muren
    addWall(1947,5604,2136,30, -1); --25
    addWall(3548,5614,158,30, 0); --26
    addWall(2903,5277,681,30, 90); --27
    addWall(2319,4935,1636,30, 1); --28

    addWall(2300,4930,1635,30, 0); --30
    addWall(2649,4816,269,30, 90); --31
    addWall(2661,4145,277,30, 90); --32
    addWall(2661,4045,1832,30, 0); --33
    addWall(1101,4053,451,30, -1); --34

    camera.x = 100;
    camera.y = 100;

    function moveCamera()
        camera.x = display.contentWidth/2 - player.getRect().x;
        camera.y = display.contentHeight/2 - player.getRect().y;
    end

    function gameLoop()
        moveCamera();
        player.tick(20/1000, walls, thermos);
    end

    gameLoopFn = gameLoop;

    --    moveCamera();
    Runtime:addEventListener("enterFrame", gameLoop);

end


-- show()
function scene:show( event )

    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then
        -- Code here runs when the scene is still off screen (but is about to come on screen)

    elseif ( phase == "did" ) then
        -- Code here runs when the scene is entirely on screen



    end
end


-- hide()
function scene:hide( event )

    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then
        -- Code here runs when the scene is on screen (but is about to go off screen)

    elseif ( phase == "did" ) then
        -- Code here runs immediately after the scene goes entirely off screen

    end
end


-- destroy()
function scene:destroy( event )

    local sceneGroup = self.view
    -- Code here runs prior to the removal of scene's view
    destroyed = true;
    print("destroyed 1");
    Runtime:removeEventListener("enterFrame", gameLoopFn);
end


-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------

return scene;


