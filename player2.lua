local collision = require "collision";
local gameTimer = require "gameTimer";
local composer = require "composer";

local player = {};
local playerEntity;

local speed = 0;
local maxSpeed = 5000;
local maxSpeedWallSlide = 250;
--local maxSpeed = 500;
local speedPerSec = 2000;
local speedPerSecWallSliding = 400;
local speedPerSecGrounded = 2000;

local speedHor = 0;
local maxSpeedHor = 9000;
local speedPerSecHor = 700;
local speedPerSecHorAir = 700;

local grounded = false;
local lastWalljumpedTS = -10000;
local wallSlidingLeft = false;
local wallSlidingRight = false;

local goingLeft = false;
local goingRight = false;

local animation;

local currentDirection = nil;

function player.create(camera)
    goingLeft = false;
    goingRight = false;

    playerEntity = display.newRect(100, 50, 60, 60);
    playerEntity:setFillColor( 1, 0, 0);
    player.reset();

    camera:insert(playerEntity);

    local sheetOptions =
    {
        width = 64,
        height = 64,
        numFrames = 36
    }

    local imageSheet = graphics.newImageSheet( "men.png", sheetOptions);
    local sequences_runningCat = {
        -- consecutive frames sequence
        {
            name = "up",
            start = 1,
            count = 8,
            time = 800,
            loopCount = 0,
            loopDirection = "forward"
        },
        {
            name = "left",
            start = 10,
            count = 8,
            time = 800,
            loopCount = 0,
            loopDirection = "forward"
        },
        {
            name = "down",
            start = 19,
            count = 8,
            time = 800,
            loopCount = 0,
            loopDirection = "forward"
        },
        {
            name = "right",
            start = 28,
            count = 8,
            time = 800,
            loopCount = 0,
            loopDirection = "forward"
        }
    }
    animation = display.newSprite( imageSheet, sequences_runningCat )
    animation:scale(3,3);
    camera:insert(animation);

    animation:setSequence( "left" );
    animation:play();

    playerEntity.alpha = 0;
    return playerEntity;
end

function player.getRect()
    return playerEntity;
end

function player.reset()
    print("reset");
    playerEntity.y = 0;
    playerEntity.x = 380;
    speed = 0;
    speedHor = 0;
end

function player.getSpeed()
    return speed;
end
function player.getSpeedHor()
    return speedHor;
end

local myText = display.newText( "Speed Vertical!", 800, 200, native.systemFont, 66 )
myText.alpha = 0;
myText:setFillColor( 1, 1, 0 );
myText:addEventListener("tap", player.reset);

local myText2 = display.newText( "Speed Hor!", 800, 280, native.systemFont, 66 )
myText2.alpha = 0;
myText2:setFillColor( 1, 0, 0 );

function player.tick(deltaMs)
--    print('=============');
    player.setSpeedPlayer(deltaMs);
    player.setPlayerLocation(deltaMs);
end

local speedVector;
local targetVectorEntity;
local objectVector;
local targetX;
local targetY;

function player.setSpeedPlayer(frameTimeMs)
    if(speedVector) then
        speedVector:removeSelf();
    end

    local usedSpeed = speedPerSec;
    if ((wallSlidingLeft or wallSlidingRight) and speed > 0) then
        usedSpeed = speedPerSecWallSliding;
    end
    if(grounded) then
        usedSpeed = speedPerSecGrounded;
    end


    local speedInc = frameTimeMs * usedSpeed / 1000;
    speed = speed + speedInc;
    if (speed > maxSpeed) then
        speed = maxSpeed;
    end
    
    if (wallSlidingLeft or wallSlidingRight) then
        if(speed > maxSpeedWallSlide) then
            speed = maxSpeedWallSlide;
        end
    end

    local speedIncHor;
    if(grounded) then
        speedIncHor = frameTimeMs * speedPerSecHor / 1000;
    else
        speedIncHor = frameTimeMs * speedPerSecHorAir / 1000;
    end
    if (not goingLeft and not goingRight) then
        local brake = 0.5;
        if(grounded) then
            brake = 1.1;
        end
        if(speedHor > 0) then
            speedIncHor = speedIncHor * -brake;
        elseif(speedHor < 0) then
            speedIncHor = speedIncHor * brake;
        end
    elseif (goingLeft) then
        speedIncHor = speedIncHor * -1;
        -- for counter steering
        if(speedHor > 0 and lastWalljumpedTS < gameTimer.getGameTime() - 1000) then
            speedIncHor = speedIncHor*2;
        end
    elseif (goingRight) then
        -- for counter steering
        if(speedHor < 0 and lastWalljumpedTS < gameTimer.getGameTime() - 1000) then
            speedIncHor = speedIncHor*2;
        end
    end
    speedHor = speedHor + speedIncHor;
    if (speedHor > maxSpeedHor) then
        speedHor = maxSpeedHor;
    end

    playerEntity.rotation = playerEntity.rotation + frameTimeMs / 1000 * speedHor * 0.3;

    if(speedVector) then
        speedVector:removeSelf();
    end

    speedVector = display.newLine( playerEntity.x, playerEntity.y, playerEntity.x + speedHor, playerEntity.y+speed);
    speedVector:setStrokeColor(1,1,1);
    speedVector.strokeWidth = 0;

    myText.text = math.round(speed);
    myText2.text = math.round(speedHor);
end

function player.setPlayerLocation(frameTimeMs)
    playerEntity.y = playerEntity.y + speed * frameTimeMs / 1000;
    playerEntity.x = playerEntity.x + speedHor * frameTimeMs / 1000;

    grounded = false;
    wallSlidingLeft = false;
    wallSlidingRight = false;

    animation.x = playerEntity.x;
    animation.y = playerEntity.y - 60;

    if(playerEntity.y > 2000) then
        composer.removeScene( "scene2" );
        composer.gotoScene( "scene1" );
    end

end

function player.setDirection(direction) -- "left"/"right"/"idle"
    goingLeft = false;
    goingRight = false;
    if (direction == "left") then
        goingLeft = true
        if(currentDirection ~= 'left') then
            print('left');
            currentDirection = 'left';
            animation:setSequence( "left" );
            animation:play();
        end
    end;
    if (direction == "right") then
        goingRight = true
        if(currentDirection ~= 'right') then
            print('right');
            currentDirection = 'right';
            animation:setSequence( "right" );
            animation:play();
        end
    end;

    if(not goingRight and not goingLeft) then
        animation:pause();
    end

end

local function putOnTop(otherObject)
    local objectX, objectY = otherObject:localToContent( 0, 0 );
    local playerX, playerY = playerEntity:localToContent( 0, 0 );
    --determine place to put the player on top of the object
    playerEntity.rotation = otherObject.rotation;

    local hellingPerX = math.sin(math.rad(otherObject.rotation)) / math.cos(math.rad(otherObject.rotation));
    local newY = (playerEntity.x - objectX) * hellingPerX;
    playerEntity.y = newY + objectY;

    -- to set the player correctly on the object, we calculate how much we need to push out horizontally,
    -- and then glide it back up to the correct X coordinate.
    local gamma = playerEntity.rotation;
    local x = playerEntity.width / 2 + otherObject.height / 2;
    local z = math.cos(math.rad(gamma)) * x;
    local y = math.sin(math.rad(gamma)) * x;

    playerEntity.y = playerEntity.y - z;
    local offset = hellingPerX * y;
    playerEntity.y = playerEntity.y - offset;
end

local function getClosestVector(speedVectorEndCoor, A, B)
    --what is closer, A or B?
    local diffA = {x = math.abs(A.x - speedVectorEndCoor.x), y = math.abs(A.y - speedVectorEndCoor.y)};
    local diffB = {x = math.abs(B.x - speedVectorEndCoor.x), y = math.abs(B.y - speedVectorEndCoor.y)};
    local distanceA = math.sqrt(math.pow(diffA.x, 2) + math.pow(diffA.y, 2));
    local distanceB = math.sqrt(math.pow(diffB.x, 2) + math.pow(diffB.y, 2));
    local closestVector;
    if(distanceA < distanceB) then
        closestVector = A;
    else
        closestVector = B;
    end
    return closestVector;
end

local function determineFallOff(angleSpeed, angleObject, lengthSpeedVector)
    local angleDiff = player.diffBetweenAngles(angleSpeed, angleObject);
    angleDiff = angleDiff / (math.pi/2);
    -- soften the angleDiff the smaller it is
    angleDiff = math.pow(angleDiff, 1.7);

    local fallOffPerAngleDiff = lengthSpeedVector / 1; --1 is the maximum anglediff
    local fallOff = (fallOffPerAngleDiff * angleDiff);
    return fallOff;
end

function player.putOnObject(otherObject, frameTimeMs)
    local objectX, objectY = otherObject:localToContent( 0, 0 );

    putOnTop(otherObject);

    local left = {
        x=objectX - otherObject.width/2,
        y=objectY
    };
    local right = {
        x=objectX + otherObject.width/2,
        y=objectY,
    };

    local A = collision.rotateRectangleVertex(otherObject, left);
    local B = collision.rotateRectangleVertex(otherObject, right);

    local speedVecX = objectX + speedHor;
    local speedVecY = objectY + speed;

    local speedVectorEndCoor = { x = speedVecX, y = speedVecY };

    local closestVector = getClosestVector(speedVectorEndCoor, A, B);

    local targetVector = {x = closestVector.x - objectX, y = closestVector.y - objectY};

    local lengthSpeedVector = math.sqrt(math.pow(speedHor, 2) + math.pow(speed, 2));

    local angleSpeed = math.atan(-speed / speedHor);
    local angleObject = math.atan(-targetVector.y / targetVector.x);

    angleSpeed = player.correctAngle(angleSpeed, -speed, speedHor);
    angleObject = player.correctAngle(angleObject, -targetVector.y, targetVector.x);

    local fallOff = determineFallOff(angleSpeed, angleObject, lengthSpeedVector);
    lengthSpeedVector = lengthSpeedVector - fallOff;

    local xTarget = math.cos(angleObject) * lengthSpeedVector;
    local yTarget = -math.sin(angleObject) * lengthSpeedVector;

    if(targetVectorEntity) then
        targetVectorEntity:removeSelf();
    end

    targetVectorEntity = display.newLine( objectX, objectY, objectX + xTarget, objectY + yTarget );
    targetVectorEntity.strokeWidth = 0;
    targetVectorEntity:setStrokeColor(0,1,0);

    speedHor = xTarget;
    speed = yTarget;


    grounded = true;
end

function player.correctAngle(angle, y, x)
    if(y < 0 and x < 0) then
        return angle + math.pi;
    elseif(y < 0 and x >= 0) then
        return angle + math.pi*2;
    elseif(y >= 0 and x < 0) then
        return angle + math.pi;
    end
    return angle;
end

function player.diffBetweenAngles(angle1, angle2)
    local phi = math.abs(angle1 - angle2) % (math.pi*2);       -- This is either the distance or 360 - distance
    local distance;
    if(phi > math.pi) then
        distance = math.pi*2 - phi;
    else
        distance = phi;
    end
    return distance;
end

function player.wallSlideObject(otherObject, dir)
    local objectX, objectY = otherObject:localToContent( 0, 0 );
    if(dir == 'left') then
        playerEntity.x = playerEntity.x-10;
--        wallSlidingLeft = true;
    elseif(dir == 'right') then
        playerEntity.x = playerEntity.x+10;
--        wallSlidingRight = true;
    end
    speedHor = 0;
    playerEntity.rotation = 0;
end

local function jumpAni2()
    transition.to(playerEntity, { xScale = 1, yScale = 1, time = 100 })
end

local function jumpAni1()
    transition.to(playerEntity, { xScale = 0.5, yScale = 1.5, time = 200, onComplete = jumpAni2 })
end

function player.jump()
    if (grounded) then
        jumpAni1();
        print("jump");
        speed = -1100;
    elseif (wallSlidingLeft) then
        jumpAni1();
        lastWalljumpedTS = gameTimer.getGameTime();
        speed = -790;
        speedHor = -300;
    elseif (wallSlidingRight) then
        jumpAni1();
        lastWalljumpedTS = gameTimer.getGameTime();
        speed = -790;
        speedHor = 300;
    end
end

return player;
