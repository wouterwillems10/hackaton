local controls = {};
local leftHold = false;
local rightHold = false;
local directionListener;
local stopListener;

local cursor = display.newRect(0,0,30,30);
cursor:setFillColor(0,1,0);

function controls.create(aDirectionListener, aStopListener)
    local uiField = display.newRect(display.contentCenterX, display.contentCenterY, display.contentWidth, display.contentHeight);
    uiField:setFillColor(0,1,0);
    uiField:addEventListener("touch", controls.touchListener);
    uiField.isVisible = false;
    uiField.isHitTestable = true;

    directionListener = aDirectionListener;
    stopListener = aStopListener;
end

function controls.isLeft()
    if (controls.isJump()) then
        return false;
    end
    return leftHold;
end

function controls.isRight()
    if (controls.isJump()) then
        return false;
    end
    return rightHold;
end

function controls.isJump()
    return rightHold and leftHold;
end

function controls.isIdle()
    return not rightHold and not leftHold;
end

function controls.touchListener(event)
    leftHold = false;
    cursor.x = event.x;
    cursor.y = event.y;
    cursor.alpha = 0;
    if (event.phase == "began" or event.phase == "moved") then
        directionListener(event.x, event.y);
    else
        stopListener();
    end
end

return controls;
