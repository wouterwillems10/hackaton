local gameTimer = {};
local gameTimeElapsed = 0;
local xSave;
local ySave;
local xTheme;

function gameTimer.setCoors(x,y, theme)
    xSave = x;
    ySave = y;
    xTheme = theme;
    print('thema?');
    print(xTheme);
end

function gameTimer.getCoors()
    return { x = xSave, y = ySave, theme = xTheme };
end

function gameTimer.addGameTime(ms)
    gameTimeElapsed = gameTimeElapsed + ms;
end

function gameTimer.getGameTime()
    return gameTimeElapsed;
end

return gameTimer;