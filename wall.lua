local wall = {};

function wall.create(x,y,w,h,a)
    local wallEntity = display.newRect(x, y, w, h);
    wallEntity.rotation = a;
    wallEntity:setFillColor(0,0,1);
    wallEntity.alpha = 0.4;
    local entity = {};

    function entity.getRect()
        return wallEntity;
    end

    return entity;
end

return wall;