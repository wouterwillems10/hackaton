--
-- created with TexturePacker (http://www.codeandweb.com/texturepacker)
--
-- $TexturePacker:SmartUpdate:6297ec2d465f75f159a833a8d87f4a25:b577279756076309be824405f5690c7d:1ab27f9675283837280b115d82fdf915$
--
-- local sheetInfo = require("mysheet")
-- local myImageSheet = graphics.newImageSheet( "mysheet.png", sheetInfo:getSheet() )
-- local sprite = display.newSprite( myImageSheet , {frames={sheetInfo:getFrameIndex("sprite")}} )
--

local SheetInfo = {}

SheetInfo.sheet =
{
    frames = {
    
        {
            -- Back-left
            x=41,
            y=1,
            width=33,
            height=53,

            sourceX = 3,
            sourceY = 0,
            sourceWidth = 39,
            sourceHeight = 55
        },
        {
            -- Back-neutral
            x=185,
            y=1,
            width=33,
            height=51,

            sourceX = 2,
            sourceY = 0,
            sourceWidth = 39,
            sourceHeight = 51
        },
        {
            -- Back-right
            x=115,
            y=1,
            width=33,
            height=52,

            sourceX = 5,
            sourceY = 1,
            sourceWidth = 41,
            sourceHeight = 54
        },
        {
            -- Front-left
            x=1,
            y=1,
            width=38,
            height=53,

            sourceX = 1,
            sourceY = 0,
            sourceWidth = 42,
            sourceHeight = 55
        },
        {
            -- Front-neutral
            x=76,
            y=1,
            width=37,
            height=52,

            sourceX = 2,
            sourceY = 1,
            sourceWidth = 41,
            sourceHeight = 54
        },
        {
            -- Front-right
            x=150,
            y=1,
            width=33,
            height=52,

            sourceX = 3,
            sourceY = 2,
            sourceWidth = 39,
            sourceHeight = 54
        },
        {
            -- Left-left
            x=360,
            y=1,
            width=31,
            height=49,

            sourceX = 4,
            sourceY = 0,
            sourceWidth = 39,
            sourceHeight = 51
        },
        {
            -- Left-neutral
            x=220,
            y=1,
            width=32,
            height=50,

            sourceX = 0,
            sourceY = 1,
            sourceWidth = 36,
            sourceHeight = 52
        },
        {
            -- Left-right
            x=317,
            y=1,
            width=41,
            height=49,

            sourceX = 2,
            sourceY = 2,
            sourceWidth = 43,
            sourceHeight = 53
        },
        {
            -- Righ-left
            x=254,
            y=1,
            width=30,
            height=50,

            sourceX = 3,
            sourceY = 2,
            sourceWidth = 34,
            sourceHeight = 52
        },
        {
            -- Right-neutral
            x=393,
            y=1,
            width=23,
            height=49,

            sourceX = 2,
            sourceY = 2,
            sourceWidth = 27,
            sourceHeight = 51
        },
        {
            -- Right-right
            x=286,
            y=1,
            width=29,
            height=50,

            sourceX = 0,
            sourceY = 1,
            sourceWidth = 29,
            sourceHeight = 52
        },
    },
    
    sheetContentWidth = 417,
    sheetContentHeight = 55
}

SheetInfo.frameIndex =
{

    ["Back-left"] = 1,
    ["Back-neutral"] = 2,
    ["Back-right"] = 3,
    ["Front-left"] = 4,
    ["Front-neutral"] = 5,
    ["Front-right"] = 6,
    ["Left-left"] = 7,
    ["Left-neutral"] = 8,
    ["Left-right"] = 9,
    ["Righ-left"] = 10,
    ["Right-neutral"] = 11,
    ["Right-right"] = 12,
}

function SheetInfo:getSheet()
    return self.sheet;
end

function SheetInfo:getFrameIndex(name)
    return self.frameIndex[name];
end

return SheetInfo
